<?php
$servername ="localhost";
$username ="root";
$password ="admin";
class Database
{
    private static $db = null;

    /**
     * Database constructor.
     */
    private function __construct()
    {

    }

    /**
     * @return null
     */
    public static function getDb()
    {
        try {
            if(is_null(self::$db))
            {
                self::$db = new PDO("mysql:host=localhost;dbname=mvcweb", 'root','admin'   );
                self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            return self::$db;
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }

    }

}
