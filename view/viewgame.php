<?php session_start(); ?>

<html lang="en">
<head>
    <title>View game</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <style>
        <?php  include __DIR__.'/../assets/stylesheets/main.css'; ?>

    </style>
</head>
<body>

    <?php
        echo '<form method="post" enctype="multipart/form-data" action="games.php"> 
                <span class="row image-txt-container bg-light border no-gutters">
                <div class="col-md-5 image-item child" >
                    <img class="w-100" src="assets/images/'.$game['image'].'" src="'.$game['image'].'"/>
                </div>    
                <div class="col-md-3 child">
                    <input type="hidden" name="cart_producer" value="' . $game['producer'].  '" />
                    <h5 class="mt-0" >'.$game['title'] .'</h5>
                    <div><strong>Producer:</strong><span class="card-text" >'. $game['producer']. '</span></div>
                    <div><strong>Price:</strong><span class="card-text">'. $game['price']. '</span></div>
                    <div><strong>Quantity:</strong>
                        <input type="number" class="quantity" name="tmp_cart_quantity" value="'.$game['quantity'] .'" size="2" />
                    <div class="bg-light">
                        <button type="submit" class="btn btn-primary" name ="cart" >
                        <i class="fa fa-shopping-cart" aria-hidden="true" style=""></i>
                        To cart</button>
                    </div>
                </div>   
             </span>
             <input type="hidden" name="form_submitted" value="1" />
             <input type="hidden" name="tmp_cart_tile" value="'.$game['title'].'" />
             </form>'
    ?>
</body>
</html>