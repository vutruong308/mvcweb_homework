<!DOCTYPE>
<html lang="en">
<head>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-md navbar-light">

    <a class="navbar-brand" href="#!">
        <img src="https://static.topcv.vn/company_covers/KOd98PUc4g50xEY2RPQq.jpg" height="30" alt="smartosc logo">
    </a>

    <!-- Collapse button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav1"
            aria-controls="basicExampleNav1" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Breadcrumbs -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a class="waves-effect" href="index.php">Home</a></li>
<!--        <li class="breadcrumb-item"><a class="waves-effect" href="#!">Templates</a></li>-->
<!--        <li class="breadcrumb-item active"><a class="waves-effect" href="#!">E-commerce</a></li>-->
    </ol>
    <!-- Breadcrumbs -->

    <!-- Links -->
    <div class="collapse navbar-collapse" id="basicExampleNav1">

        <!-- Right -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
<!--                    <a href="view/viewcart.php"  class="nav-link navbar-link-2 waves-effect"><i class="fa fa-shopping-cart pl-0"></i> Cart-->
<!--                        <span>--><?php //'.$cart_count.'; ?><!--</span>-->
                    </a>
                    <?php
                    if(!empty($_SESSION["shopping_cart"])) {
                        $cart_count = count(array_keys($_SESSION["shopping_cart"]));
                        echo'
                             ';
                    } ?>
                </a>
            </li>

<!--            <li class="nav-item dropdown">-->
<!--                <a class="nav-link dropdown-toggle waves-effect" id="navbarDropdownMenuLink3" data-toggle="dropdown"-->
<!--                   aria-haspopup="true" aria-expanded="true">-->
<!--                    <i class="united kingdom flag m-0"></i>-->
<!--                </a>-->
<!--                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">-->
<!--                    <a class="dropdown-item" href="#!">Action</a>-->
<!--                    <a class="dropdown-item" href="#!">Another action</a>-->
<!--                    <a class="dropdown-item" href="#!">Something else here</a>-->
<!--                </div>-->
<!--            </li>-->
            <li class="nav-item">
                <a href="#!" class="nav-link waves-effect">
                    Shop
                </a>
            </li>
            <li class="nav-item">
                <a href="#!" class="nav-link waves-effect">
                    Contact
                </a>
            </li>
            <li class="nav-item">
                <a href="#!" class="nav-link waves-effect">
                    Sign in
                </a>
            </li>
            <li class="nav-item pl-2 mb-2 mb-md-0">
                <a href="#!" type="button"
                   class="btn btn-outline-info btn-md btn-rounded btn-navbar waves-effect waves-light">Sign up</a>
            </li>
        </ul>

    </div>
    <!-- Links -->

</nav>
<!-- Navbar -->
</body>
</html>