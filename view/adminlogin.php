<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="col-md-3"> </div>
        <form action="" method="post" >
            <div class="form-group col-md-3">
                <label >Name</label>
                <input  class="form-control" type="text" name="username" required="required">
            </div>
            <div class="form-group col-md-3">
                <label >Password</label>
                <input class="form-control" type="password" name="password" required="required">
            </div>
            <div class="form-group col-md-1">
                <button class="form-control" type="submit" id ="btn_submit" >Login</button>
            </div>
        </form>
    </div>
</body>
</html>
