
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/stylesheets/main.css">
</head>
<body>

<?php include 'view/header.php';
    if (isset($_SESSION['cart']))
        $sum_cart = 0;
    ?>
<div class="message-box">
    <?php echo $result;
    ?>
</div>
<div class="cart">
 <table class="table">
        <tbody>
        <tr>
            <td>Image</td>
            <td>Game</td>
            <td>Producer</td>
            <td>Quantity</td>
            <td>Price</td>
            <td>Item Total</td>
        </tr>
        </tbody>
        <?php
        foreach($_SESSION["cart"] as $game){
        echo '<tr>
            <td>
                <img src="assets/images/'.$game->getImage().'" alt="'.$game->getImage().'">
                <td>'. $game->getTitle().' <br/> </td>
                <td>'. $game->getProducer().'<br/> </td>
                <td>'. $game->getQuantity().'<br/> </td>
                <td>'. $game->getPrice().'<br/> </td>
                <td>'. $game->getQuantity() * $game->getPrice().'<br/> </td>
            </td>
        </tr>';
        $sum_cart = $sum_cart + ($game->getQuantity() * $game->getPrice());
        }?>
    </table>
    <hr/>
    <div class="sumcart float-lg-right" style="margin: 0px 5%;">
        <strong>Total:</strong> <?php echo $sum_cart ;?>
    </div>
</div>
</body>
</html>