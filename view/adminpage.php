
<!DOCTYPE html>
<html lang="en">
<head>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
       integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <?php
    session_start();
    if(!$_SESSION['isLogged'] == 1 &&  !isset($_SESSION['isLogged']))
        header('Location:/mvcweb/view/adminlogin.php');
    else
        echo '<h6 class="col-md-3">Welcome come <strong>'.$_SESSION['username'].'</strong></h6>';
    ?>
 <form method="post" action="../mvcweb/games.php" enctype="multipart/form-data" >
     <div class="form-group row">
      <label for="title" class="col-md-1">Title</label>
      <input type="text" name="title" required="required" class="form-control col-md-2">
     </div>
     <div class="form-group row">
      <label class="col-md-1">Producer</label>
      <input type="text" name="producer" required="required" class="form-control col-md-2">
     </div>
     <div class="form-group row">
      <label class="col-md-1">Price</label>
      <input type="text" name="price" required="required" class="form-control col-md-2">
     </div>
      <div class="form-group row">
       <label class="col-md-1">Quantity</label>
       <input type="number" name="quantity" class="form-control col-md-2">
      </div>
     <div class="form-group row">
         <label class="col-md-1">Image</label>
         <input type="file" name="image" class="form-control col-md-2">
     </div>
  <div class="form-group">
   <button type="submit" name="add" class="btn btn-primary col-md-1" id="btn_add" >Add</button>
  </div>
 </form>
</div>
<hr />
 <div class="container"  style="margin:10px auto; " >
    <form enctype="multipart/form-data" method="post" action="" >
    <div class="form-group row">
        <label class="col-form-label custom-control-inline">Select File</label>
        <input type="file" name="file" class="form-control col-md-2 bg-light">
    </div>
    <div class="form-group row">
        <label class="col-form-label custom-control-inline" >Import data</label>
        <button  class="btn btn-primary col-md-2" name="import">Import </button>
    </div>
    </form>
</div>
    <?php
    echo '<div class="alert alert-danger bg-white " style="border: none;" >'.$result.'</div>';
    ?>


</body>
</html>
