<?php
    require_once (__DIR__. '/../model/E_Game.php');
    require_once (__DIR__. '/../model/M_Game.php');
    class GamesController
    {
        public $db;

        /**--
         * GamesController constructor.
         * @param $db
         */
        public function __construct()
        {
            $this->db = new M_Game();
        }

        //Get All Games
        public function viewAll()
        {

            $games = $this->db->getAllGames();
            include 'view/gameslist.php';

        }

        //Get One specific game
        public function view()
        {
            $game = $this->db->getGame($_GET['title']);
            include 'view/viewgame.php';
        }

        //Add Game to db
        public function addGame()
        {
            $title = $_POST['title'];
            $producer = $_POST['producer'];
            $price = $_POST['price'];
            $quantity = $_POST['quantity'];
            $image = $_FILES['image'];

            $result = $this->db->checkimg($_FILES['image']);
            if ($result!="")
            {
                $result;
                include 'view/adminpage.php';
            }
            else {
                $data = new Game($title, $producer, $price, $quantity, $image);
                $result = $this->db->addGame($data);
                if ($result) {
                    $result = "Add success";
                    echo "<script type='javascript'>alert('$result');</script>";
                    include 'index.php';
                } else {
                    $result = "Error, add fail";
                    include 'view/adminpage.php';
                }
            }
        }

        //Handle when customer add product to cart
        public function cartGame(){

            $cartarray = array();
            $title = $_POST['tmp_cart_tile'];
            $quantity = $_POST['tmp_cart_quantity'];
            $result = $this->db->getGame($title);

            array_push($cartarray,
                new Game($result['title'], $result['producer'],$result['price'], $quantity, $result['image']));

            //Check if cart is empty, create a new one
            if(empty($_SESSION['cart'])){
                $_SESSION['cart'] = array();
                $_SESSION['cart'] = $cartarray;
            }
            else{
                //Check if duplicate, then auto add
                foreach ($_SESSION['cart'] as $game){
                    if ($game->getTitle() == $cartarray[0]->getTitle()){
                        $tmp = $game->getQuantity() + $cartarray[0]->getQuantity();
                        $game->setQuantity($tmp) ; //add to exist quantity
                    }
                    else
                        $_SESSION['cart'] = array_merge($_SESSION['cart'], $cartarray); //merge existing cart with new one.
                }
            }
            $result = "<div class='box'>Add to cart success </div>";
            include ('view/viewcart.php');
            session_start();

        }

        //Wong
        public function logout(){
            if (isset($_SESSION['isLogged']))
                unset($_SESSION['isLogged']);
        }




    }


    //debug
    //    $tmp = new GamesController();
//    $_POST['title'] = 'Superman';
//    $_POST['producer'] = 'Capcom';
//    $_POST['price'] = '100';
//    $_POST['quantity'] = '10';
//    $dir = "/home/vutch308/Pictures/";
////    $dir = "/home/vutch308/Videos/";
//    $_FILES['image'] = scandir($dir)[2];
//  $tmp->addGame();
//    var_dump($tmp);
