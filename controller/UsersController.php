<?php

require_once (__DIR__. '/../model/M_User.php');
require_once (__DIR__.'/../model/M_Game.php');
class UsersController
{
    private $controller;
    private $controller_game;
    /**
     * UsersController constructor.
     * @param $controller
     */
    public function __construct()
    {
        $this->controller = new M_User();
        $this->controller_game = new M_Game();
    }

    //For login at admin page
    public function  adminLogin(){
        session_start();
        $commonErr ='';

        //Check stuff
        if (empty($_POST['username']))
            $commonErr = "Username is required";
        else if (empty($_POST['password']))
            $commonErr = "Password is required";
        else if (preg_match('/[^A-Za-z0-9\-]/', $_POST['username'])
            || preg_match('/[^A-Za-z0-9\-]/',$_POST['password']))
            $commonErr ="Username, password invalid";

        //If any error, output $commonErr
        if (!empty($commonErr))
        {
            echo $commonErr;
            include 'view/adminlogin.php';
        }
        else {
            $user = $this->controller->getUser($_POST['username'], $_POST['password']);
        }

        if ($user == true)
        {
            $_SESSION['isLogged'] = $user->getIsLogged();
            $_SESSION['username'] = $user->getUsername();
            include 'view/adminpage.php';
        }
    }


    public function checkIsLogged()
    {
        if (isset($_SESSION['isLogged']) && $_SESSION['isLogged'] == 1)
        {
            echo "Success";
            header('Location:adminpage.php');
        }
        else
        {
            include 'view/adminlogin.php';
        }
    }

    //Import File
    public function uploadFile()
    {
        $filename=$_FILES["file"]["tmp_name"];
        if($_FILES["file"]["size"] > 0 ) {
            $i = 0;
            $file = fopen($filename,"r");
            while (($data = fgetcsv($file, 10000, ",")) !== FALSE)
            {
                //Skip first row
                if ($i > 0) {
                    $game = new Game($data[0], $data[1], (int)$data[2], (int)$data[3], "");
                    $result = $this->controller_game->addGame($game);
                }
                $i++;
                if($result != "")
                {
                    $result;
                }
                else{
                    $result = "Success";
                }
            }
            fclose($file);
            header('Location:index.php');
        }
    }

    //to return /adminlogin.
    public function adminLoginFail(){
        include 'view/adminlogin.php';
    }
}


//debug
//$tmp = new UsersController();
//$_POST['username'] = 'vu308';
//$_POST['password'] = '123';
//$tmp->adminLogin();
////$tmp->adminLogin('vu308', '123');
//var_dump($_SESSION['isLogged']);
