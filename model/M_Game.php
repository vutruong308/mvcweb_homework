<?php
    require_once("E_Game.php");
    require_once(__DIR__."/../connection.php");
    class M_Game
    {

        public function getAllGames()
        {
            $sql = "SELECT * FROM game WHERE quantity > 0";
            $req = Database::getDb()->prepare($sql);
            $req->execute();
            return $req->fetchAll() ;
        }

        //Insert a game to database
        public function addGame(Game $item)
        {
            $this->uploadimg($item->getImage());
            $sql = "INSERT INTO game(title,producer,price,quantity, image) 
                VALUES('".$item->getTitle()."','".$item->getProducer()."','".$item->getPrice()."','".$item->getQuantity()."','".$item->getImage()['name']."');";

            try{
                $req = Database::getDb()->prepare($sql);
                $result = $req->execute();
            }

            catch (PDOException $e) { var_dump($e); return $result = false;}

            return $result;

        }

        public function getGame($title)
        {
            $allGame = $this->getAllGames();
            foreach ($allGame as $game)
            {
                if ($game['title'] == $title)
                    return $game;
            }
        }

        //process image
        public function checkimg($image){
            $target_dir = __DIR__."/assets/images/";
            $target_file = $target_dir.basename($image['name']);

            $error_type = "";
            $img_type = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            $image['size'];

            //check if a real image or fake image with extension
            $check = getimagesize($image['tmp_name']);
            if ($check === false )
            {
                $error_type= "This is not an image";
            }

            //check if image too large
            if($image['size'] > 5000000) //50 MB
            {
                $error_type = "This image is too large.";
            }

            //check if image belong to file format
            if($img_type != "jpg" && $img_type != "gif" && $img_type != "png" && $img_type != "jpeg")
            {
                $error_type = "Doesn't support this format. Only jpg, gif, png, jpeg are allowed";
            }

            return $error_type;
        }

        public function uploadimg($image){
            $target_dir = __DIR__."/../assets/images/";
            $target_file = $target_dir.basename($image['name']);
            move_uploaded_file($image['tmp_name'], $target_file);
        }

    }

