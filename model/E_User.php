<?php
class E_User{
    protected $username;
    protected $password;
    protected $isLogged;

    /**
     * E_User constructor.
     * @param $username
     * @param $password
     * @param $role
     */
    public function __construct($username, $password, $isLogged)
    {
        $this->username = $username;
        $this->password = $password;
        $this->isLogged = $isLogged;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getIsLogged()
    {
        return $this->isLogged;
    }

    /**
     * @param mixed $isLogged
     */
    public function setIsLogged($isLogged): void
    {
        $this->isLogged = $isLogged;
    }


}