<?php
class Game
{
    protected  $title;
    protected $producer;
    protected $price;
    protected $quantity;
    protected  $image;

    public function __construct($_title, $_producer,$_price,$_quantity, $_image)
    {
        $this->title = $_title;
        $this->producer = $_producer;
        $this->price = $_price;
        $this->quantity = $_quantity;
        $this->image = $_image;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getProducer()
    {
        return $this->producer;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }
}

