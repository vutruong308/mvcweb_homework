<?php
require ('E_User.php');
require_once(__DIR__."/../connection.php");

class M_User
{
    public function getAllUsers(){

        $userList = array();
        $sql = "SELECT * FROM user ";
        $query = Database::getDb()->prepare($sql);
        $query->execute();
        foreach ($query->fetchAll() as $data){
            $item = new E_User($data[1], $data[2],$data[3]);
            array_push($userList,$item);
        }
        return $userList;
    }

    public function getUser($username, $password){

        $userList = $this->getAllUsers();
        $password = hash("sha256",$password);
        foreach ($userList as $user)
        {
            if ($username == $user->getUsername()  && $password == $user->getPassword())
            {
                $user->setIsLogged(1);
                return $user;
            }
        }
        return false;
    }
}

